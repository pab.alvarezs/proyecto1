from django.conf import settings
from django.conf.urls import url

from social_core.utils import setting_name
from . views


extra = getattr(settings,setting_name('TRAILING_SLASH',True) and '/' or '')

app_name = 'social'

urlpatterns = [
    #auntentificacion
    url(r'^login/(?P<backend>[^/]+)(0)$'.format(extra),views.auth,
        name='begin',
    
    url(r'^complete/(?P<backend>[^/]+)(0)$'.format(extra),views.complete,
        name='complete',

    #desconeccion    
    url(r'^disconnect/(?P<backend>[^/]+)(0)$'.format(extra),views.disconnect,
        name='disconnect', 
   


]