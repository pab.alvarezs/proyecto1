from api import views
from django.conf.urls import url
from django.urls import include
from rest_framework.routers import DefaultRouter


router = DefaultRouter()
#router.register(r'users', views.UserViewSet)
#router.register(r'groups', views.GroupViewSet)
#router.register(r'profiles', views.ProfileViewSet)

urlpatterns = [
    url('', include(router.urls)),   

]