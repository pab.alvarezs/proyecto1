from django.contrib.auth.models import User, Group # importamos modelos django
from rest_framework import serializers
from .models import Profile 

#creamos serializers para usuarios y grupos
class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'groups')


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ('url', 'name')


class ProfileSerializer(serializers.HyperlinkedModelSerializer):
    # user = UserSerializer(required=True)
    class Meta:
        model=Profile
        fields=('url', 'user', 'email')

    def create(self, validated_data):
        """
        Overriding the default create method of the Model serializer.
        :param validated_data: data containing all the details of student
        :return: returns a successfully created student record
        """
        user_data=validated_data.pop('user')
        user=UserSerializer.create(
            UserSerializer(), validated_data=user_data)
        profile, created=Profile.objects.update_or_create(user=user,
                                                            subject_major=validated_data.pop('subject_major'))
        return profile
