from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    #email = models.TextField()
    run = models.TextField()
    nombre_completo = models.TextField()
    fecha_nac = models.DateField()
    tel = models.IntegerField(blank=True,null=True)
    region = models.IntegerField(blank=True,null=True)
    comuna = models.IntegerField(blank=True,null=True)
    tipo_vivienta = models.IntegerField()
    contraseña = models.TextField()

    def save(self, *args, **kwargs):
        user = User.objects.create_user(username=self.email,email=self.email,password=self.contraseña)
        user.save()
        self.user = user
        super().save(**args, **kwargs)

    def __str__(self):
        return 

    def __unicode__(self):
        return 
