from django.db import models
from django.utils import timezone

#modelo usuario
class Usuario(models.Model):
    rut = models.CharField(max_length=200)
    nombreCompleto = models.CharField(max_length=200)
    fecha_naciemiento = models.DateTimeField(blank=True, null=True)
    telefono =  models.IntegerField()
    e_mail = models.CharField(max_length=200)
    region = models.CharField(max_length=200)
    ciudad = models.CharField(max_length=200)
    tipo_vivienda = models.CharField(max_length=200)
    

    def _str_(self):
        return self.nombreCompleto    


#modelo rescatado
class Rescatado(models.Model):
    nombre = models.TextField(max_length=50)
    raza = models.TextField(max_length=20)
    descripcion  = models.TextField(max_length=200)
    estado = models.TextField(max_length=20)
    file = models.FileField(blank=True, null=True, upload_to="rescatados")

    def _str_(self):
        return self.nombre
    def create(self):
        self.save()
    

