from django.shortcuts import render, redirect
from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.utils import timezone
from django.template import loader
from django.urls import reverse
from contacto.models import Usuario, Rescatado
from django.utils import timezone
#from django.views.generic import CreateView, ListView
from django.contrib.auth import authenticate, login, logout

# Create your views here.

#vista login

#vista pagina principal con registro
def home(request):
    return render(request, 'contacto/home.html')

#vista pagina principal con registro
def home_sin_registro(request):
    return render(request, 'contacto/home_sin_registro.html')

#vista inscribir usuario
def vista_formulario_contacto(request):
   
    if request.method == 'GET':
        return render(request, 'contacto/usuarios_formularios.html', {})
    elif request.method == 'POST':
        message = ""

        try:
            usuario = Usuario()
            usuario.rut = request.POST.get('run')
            usuario.nombreCompleto = request.POST.get('nombre')
            usuario.fecha_naciemiento = request.POST.get('nacimiento')
            usuario.telefono = request.POST.get('telefono')
            usuario.e_mail = request.POST.get('email')
            usuario.region = request.POST.get('region')
            usuario.ciudad = request.POST.get('ciudad')
            usuario.tipo_vivienda = request.POST.get('tipo_casa')
            usuario.fecha_creacion = timezone.now

            
        
            #post.published_date = lambda published_date : None if published_date == "" else request.POST.get('published_date')
            usuario.save()
            message = "Éxito al guardar el usuario"
            return HttpResponseRedirect(reverse('contacto:detail', args=(usuario.pk,)))
        except Exception as e:
            message = "Error al guardar el usuario: " + str(e)
            return render(request, 'contacto/usuarios_formularios.html', {'message': message})
    else:
        response = HttpResponse('Método no permitido')
        response.status_code = 405
        return response

   

    return render(request, 'contacto/usuarios_formularios.html')

#vista ingresar rescatado
def vista_formularioRescatados(request):
   
    if request.method == 'GET':
        return render(request, 'contacto/rescatados_formulario.html', {})
    elif request.method == 'POST':
        message = ""

        try:
            rescatado = Rescatado()
            rescatado.nombre = request.POST.get('nombre')
            rescatado.raza = request.POST.get('raza')
            rescatado.descripcion = request.POST.get('descripcion')
            rescatado.estado = request.POST.get('estado')
                      
        
            #post.published_date = lambda published_date : None if published_date == "" else request.POST.get('published_date')
            rescatado.save()
            file = request.FILES['file']
            ext = file.name.split(".")[-1]
            file.name = str(rescatado.pk) + "." + ext
            rescatado.file = file
            rescatado.save()

            message = "Éxito al guardar el rescatado"
            return HttpResponseRedirect(reverse('contacto:detail', args=(rescatado.pk,)))
        except Exception as e:
            message = "Error al guardar el rescatado: " + str(e)
            return render(request, 'contacto/rescatados_formulario.html', {'message': message})
    else:
        response = HttpResponse('Método no permitido')
        response.status_code = 405
        return response

   

    return render(request, 'contacto/rescatados_formulario.html')

 
#vista listar
def vista_listarRescatados(request):
    rescatados = Rescatado.objects.all()  #esto es un queryset
    contexto = {'rescatados': rescatados}  #le damos a nuestro diccionario el queryset
    return render(request,'contacto/rescatados_listar.html', contexto) #mandamos a la plantilla el queryset


#vista modificar
def vista_modificarRecatados(request,id_rescatado):  #recibe la id de mascota
    rescatado = Rescatado.objects.get(id=id_rescatado) #queryset nos trae el rescatado con la id select
    if request.method =='GET':
        
        #escribimos los datos en el template, exportamos el contexto al template como objeto
        contexto = {'rescatado': rescatado}

    else:
        
        rescatado.nombre = request.POST.get('nombre')           #escribimos los datos del formulario, en el objecto rescatado
        rescatado.raza = request.POST.get('raza')
        rescatado.descripcion = request.POST.get('descripcion')
        rescatado.estado = request.POST.get('estado')
        rescatado.save()                                        #guardamos los datos que escribimos en el template con POST

       
        return redirect('listarRescatados')

    return render(request,'contacto/rescatados_formulario.html', contexto)

def vista_eliminarRescatado(request,id_rescatado):
    rescatado = Rescatado.objects.get(id=id_rescatado) #queryset nos trae el rescatado con la id select
    contexto = {'rescatado': rescatado}
    if request.method =='POST':
        rescatado.delete()
        return redirect('listarRescatados')
    
    return render(request,'contacto/rescatado_eliminar.html', contexto)

    
