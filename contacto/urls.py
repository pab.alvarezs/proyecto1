from django.urls import path
from django.conf.urls import include, url
from django.shortcuts import render
from . import views
from django.conf.urls.static import static
from django.conf import settings
from django.contrib.auth.views import LoginView, LogoutView

urlpatterns = [
url(r'^$', views.home_sin_registro, name='home_sin_registro'),
url(r'^registrado', views.home, name='home'),
#url(r'contacto', views.formulario),
path('contacto', views.vista_formulario_contacto, name='contacto'),
path('rescatados', views.vista_formularioRescatados, name='rescatados'),  
#path('listarRescatados/', views.vista_listarRescatados.as_view(), name='listarRescatados'),
url('listarRescatados', views.vista_listarRescatados, name='listarRescatados'), #vista listar rescatados
url(r'^editarRescatado/(?P<id_rescatado>\d+)/$',views.vista_modificarRecatados, name='rescatados_editar'), #la expresion regular permite enviar la id del rescatado selecccionado
url(r'^eliminarRescatado/(?P<id_rescatado>\d+)/$',views.vista_eliminarRescatado, name='rescatados_eliminar'), #la expresion regular permite enviar la id del rescatado selecccionado
path('login/', LoginView.as_view(template_name='contacto/login.html'), name="login"),
url(r'^logout/$',LogoutView.as_view(), name='logout'),


]